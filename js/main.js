$(document).ready(function(){
  
  $("nav > ul > li").mouseover(function(e) {
    $(this).children("ul").show();
  });
  $("nav > ul > li").mouseout(function(e) {
    $(this).children("ul").hide();
  });
  
  
  /* Scroll do wybranej pozycji w menu */
  $("nav a").click(function(e) {
    e.preventDefault();
    
    var our_target = $(this).data("target");
    var top = $("#"+our_target).offset().top;
    
    $("body").animate({
      scrollTop: top
    },500, function(){});

  });
  
})